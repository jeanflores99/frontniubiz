import { GetTodos } from "@src/axios/GetTodos";
import { useEffect } from "react";
export const useGetAllTodos = () => {
  useEffect(() => {
    GetTodos()
      .then((res) => console.log(res.data))
      .catch((err) => console.log(err));
  }, []);
};
