import { ReactNode } from "react";

interface Iprops {
  children: ReactNode | JSX.Element;
}
export const Container = ({ children }: Iprops) => (
  <div className="container mx-auto">{children}</div>
);
