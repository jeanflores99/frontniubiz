import { InputHTMLAttributes } from "react";

interface Iprops extends InputHTMLAttributes<HTMLInputElement> {
  title: string;
  onChange: any;
}
export const Input = ({ title, onChange, ...props }: Iprops) => {
  return (
    <div className="flex flex-row justify-between">
      <div className="w-1/2">{title}</div>
      <input
        className="outline-none border w-1/2"
        onChange={onChange}
        {...props}
      />
    </div>
  );
};
