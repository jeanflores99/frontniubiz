import { ENV } from "@src/environment";
import { useGetAllTodos } from "@src/services/todos/useGetAllTodos";
import { Container } from "@src/components/Layouts";
import { Input } from "@src/components/Common";
import { useFormik } from "formik";

export default function Home() {
  // useGetAllTodos();
  const { values, handleChange, handleSubmit } = useFormik({
    initialValues: { importe: "", numero: "", concepto: "", fecha: "" },
    onSubmit: (payload) => {
      console.log(payload);
    },
  });
  return (
    <Container>
      <form className="flex flex-col gap-4" onSubmit={handleSubmit}>
        <h1 className="text-2xl">Pago con Visa</h1>
        <h3 className="text-xl">Información de Pago</h3>
        <Input
          title="Importe a Pagar:"
          onChange={handleChange}
          value={values.importe}
          name="importe"
        />
        <Input
          title="Número de pedido:"
          onChange={handleChange}
          value={values.numero}
          name="numero"
        />
        <Input
          title="Concepto:"
          onChange={handleChange}
          value={values.concepto}
          name="concepto"
        />
        <Input
          type="date"
          title="Importe a Pagar:"
          onChange={handleChange}
          value={values.fecha}
          name="fecha"
        />
        <button type="submit" className="bg-green-500 text-white p-2 rounded">
          Enviar
        </button>
      </form>
    </Container>
  );
}
