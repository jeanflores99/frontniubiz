import { BaseRequest } from "./baseRequest";
const Pathname = "todos";
export const GetTodos = async () => {
  const request = BaseRequest();
  return await request.get(Pathname);
};
