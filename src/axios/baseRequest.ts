import axios from "axios";
import { ENV } from "@src/environment";

export const BaseRequest = () =>
  axios.create({
    baseURL: ENV.uri,
    headers: { "Content-Type": "application/json" },
  });
